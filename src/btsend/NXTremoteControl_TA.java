package btsend;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;



import lejos.pc.comm.NXTConnector;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
//import lejos.pc.comm.NXTInfo;

//import lejos.nxt.*;
//import lejos.nxt.remote.NXTCommRequest;
import lejos.nxt.remote.NXTCommand;
//import static lejos.nxt.remote.NXTProtocol.MOTORON;
//import static lejos.nxt.remote.NXTProtocol.REGULATION_MODE_MOTOR_SPEED;
import lejos.nxt.remote.RemoteMotor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.SensorPort;
//import lejos.nxt.remote.RemoteSensorPort;



public class NXTremoteControl_TA extends JFrame
{
  public static JButton quit, connect;
  public static JButton forward,reverse, leftTurn, rightTurn, stop, speedUp, slowDown;
  public static JLabel L1,L2,L3,L4,L5,L6,L7,L8,L9,L10;
  public static ButtonHandler bh = new ButtonHandler();
  public static NXTConnector link;
  public static RemoteMotor motorA; 
  public static RemoteMotor motorC;
  public static UltrasonicSensor ultraSonicSensor;
  
  public static int speed =50, turnSpeed = 50,speedBuffer, speedControl;
  public static int commandValue,transmitReceived;
  public static boolean[] control = new boolean[6];
  public static boolean[] command = new boolean[6];
  
  public static boolean state = false; 
  
  public NXTremoteControl_TA()
  { 
    setTitle ("Control");
    setBounds(650,350,500,500);
    setLayout(new GridLayout(4,5));
   
    L1 = new JLabel("");
    add(L1); 
    forward = new JButton("Forward");
    forward.addActionListener(bh);
    forward.addMouseListener(bh);
    forward.addKeyListener(bh);
    add(forward);
    L2 = new JLabel("");
    add(L2);
    L3 = new JLabel("");
    add(L3);
    speedUp = new JButton("Accelerate");
    speedUp.addActionListener(bh);
    speedUp.addMouseListener(bh);
    speedUp.addKeyListener(bh);
    add(speedUp);

    leftTurn = new JButton("Left");
    leftTurn.addActionListener(bh);
    leftTurn.addMouseListener(bh);
    leftTurn.addKeyListener(bh);
    add(leftTurn);
    stop = new JButton("Stop");
    stop.addActionListener(bh);
    stop.addMouseListener(bh);
    stop.addKeyListener(bh);
    add(stop);
    
    rightTurn = new JButton("Right");
    rightTurn.addActionListener(bh);
    rightTurn.addMouseListener(bh);
    rightTurn.addKeyListener(bh);
    add(rightTurn);
    L4 = new JLabel("");
    add(L4);
    slowDown = new JButton("Decelerate");
    slowDown.addActionListener(bh);
    slowDown.addMouseListener(bh);
    slowDown.addKeyListener(bh);
    add(slowDown);
    
    L5 = new JLabel("");
    add(L5);
    reverse = new JButton("Reverse");
    reverse.addActionListener(bh);
    reverse.addMouseListener(bh);
    reverse.addKeyListener(bh);
    add(reverse);
    
    L6 = new JLabel("");
    add(L6);
    L7 = new JLabel("");
    add(L7);
    L8 = new JLabel("");
    add(L8);

    connect = new JButton(" Connect ");
    connect.addActionListener(bh);
    connect.addKeyListener(bh);
    add(connect);

    L9 = new JLabel("");
    add(L9);
    L10 = new JLabel("");
    add(L10);
   
    quit = new JButton("Quit");
    quit.addActionListener(bh);
    add(quit);

  }
  
  public static void main(String[] args)
  {
     NXTremoteControl_TA NXTrc = new NXTremoteControl_TA();
     NXTrc.setVisible(true);
     NXTrc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
     
     while(state)
    { 
        control = checkCommand();
        speedControl = getSpeed(control);
        move(control, speedControl);
    }
  }//End main
  
  private static class ButtonHandler implements ActionListener, KeyListener, MouseListener 
  {
      //***********************************************************************
      //Buttons action
      public void actionPerformed(ActionEvent ae)
      {
        if(ae.getSource() == quit)  
        {
            disconnect();
            System.exit(0);
        }
        if(ae.getSource() == connect) 
        {
            connect();
        }
      
        if(ae.getSource() == speedUp) {System.out.println(6);transmitReceived = 6;}
        if(ae.getSource() == slowDown) {System.out.println(7);transmitReceived = 7;}
        //outData.flush(); //This forces any buffered output bytes to be written out to the stream.
        
      }//End ActionEvent(for buttons)

      //***********************************************************************
      //Mouse actions
   
      public void mouseClicked(MouseEvent arg0) {}

      public void mouseEntered(MouseEvent arg0) {}

      public void mouseExited(MouseEvent arg0) {}

      public void mousePressed(MouseEvent moe) 
      {
          if(moe.getSource() == forward){System.out.println("1");transmitReceived = 1;}
          if(moe.getSource() == reverse){System.out.println("2");transmitReceived = 2;}
          if(moe.getSource() == leftTurn){System.out.println("3");transmitReceived = 3;}
          if(moe.getSource() == rightTurn){System.out.println("4");transmitReceived = 4;}
          if(moe.getSource() == speedUp){System.out.println("6");transmitReceived = 6;}
          if(moe.getSource() == slowDown){System.out.println("7");transmitReceived = 7;}
          
  
      }//End mousePressed

      public void mouseReleased(MouseEvent moe) 
      {
          if(moe.getSource() == forward ||
                  moe.getSource() == reverse ||
                  moe.getSource() == leftTurn ||
                  moe.getSource() == rightTurn)
          {
              System.out.println("5");
              transmitReceived = 5;
          }
          if(moe.getSource() == slowDown)
          {
              System.out.println("60");
              transmitReceived = 60;
          }
          if(moe.getSource() == speedUp)
          {
              System.out.println("70");
              transmitReceived = 70;
          }
      }//End mouseReleased

      //***********************************************************************
      //Keyboard action
      public void keyPressed(KeyEvent ke) {}//End keyPressed

      public void keyTyped(KeyEvent ke) 
      {
          if(ke.getKeyChar() == 'w'){System.out.println(1);transmitReceived = 1;}
          if(ke.getKeyChar() == 's'){System.out.println(2);transmitReceived = 2;}
          if(ke.getKeyChar() == 'a'){System.out.println(3);transmitReceived = 3;}
          if(ke.getKeyChar() == 'd'){System.out.println(4);transmitReceived = 4;}
          if(ke.getKeyChar() == 'i'){System.out.println(6);transmitReceived = 6;}
          if(ke.getKeyChar() == 'k'){System.out.println(7);transmitReceived = 7;}
          //outData.flush();
      }//End keyTyped
   
      public void keyReleased(KeyEvent ke) 
      {
          if(ke.getKeyChar() == 'w'){System.out.println(10);transmitReceived = 10;}
          if(ke.getKeyChar() == 's'){System.out.println(20);transmitReceived = 20;}
          if(ke.getKeyChar() == 'a'){System.out.println(30);transmitReceived = 30;}
          if(ke.getKeyChar() == 'd'){System.out.println(40);transmitReceived = 40;}
          if(ke.getKeyChar() == 'i'){System.out.println(60);transmitReceived = 60;}
          if(ke.getKeyChar() == 'k'){System.out.println(70);transmitReceived = 70;}
          if(ke.getKeyChar() == 'q'){System.exit(0);}
          //outData.flush();
      }//End keyReleased
  }//End ButtonHandler
  
  public static void connect() 
  {
      
      try {
          NXTComm nxtComm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
          NXTInfo nxtInfodata = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXTBI1", "00:16:53:0B:57:FB");
          //NXTInfo[] nxtInfo = nxtComm.search(nxtInfodata.name);
          boolean connected = nxtComm.open(nxtInfodata);//(nxtInfo[0]);
          state = true;
          
          NXTCommand command = new NXTCommand(nxtComm);
          //command.//.setNXTComm(nxtComm);
          motorA = new RemoteMotor(command, 0 /*Port.A*/);
          motorC = new RemoteMotor(command, 2 /*Port.C*/);
                  
          ultraSonicSensor = new UltrasonicSensor(SensorPort.S1);
          System.out.println(ultraSonicSensor.getDistance());
          //RemoteSensorPort UltraSound = new RemoteSensorPort(command, 0 /*Port.A*/);
          //UltraSound.i2cEnable(1);
          //System.out.println(UltraSound.readRawValue());
          
          
      } //End connect
      catch (NXTCommException ex) {
          Logger.getLogger(NXTremoteControl_TA.class.getName()).log(Level.SEVERE, null, ex);
      }
     
    
  }
  
  public static void disconnect()
  {
     try
     {
        state = true;
        motorA.stop();
        motorC.stop();
     } 
     catch (Exception ioe) 
     {
        System.out.println("\nIO Exception writing bytes");
     }
     System.out.println("\nClosed data streams");
  }//End disconnec
  
  public static boolean[] checkCommand()//check input data
 {
    
      if(transmitReceived == 1) {command[0] = true;}//forward
      if(transmitReceived == 10){command[0] = false;}
      if(transmitReceived == 2) {command[1] = true;}//backward
      if(transmitReceived == 20){command[1] = false;}
      if(transmitReceived == 3) {command[2] = true;}//leftTurn
      if(transmitReceived == 30){command[2] = false;}
      if(transmitReceived == 4) {command[3] = true;}//rightTurn
      if(transmitReceived == 40){command[3] = false;}
      if(transmitReceived == 5) 
      {
          command[0] = false;//stop
          command[1] = false;
          command[2] = false;
          command[3] = false;
      }
      if(transmitReceived == 6) {command[4] = true;}//speed up
      if(transmitReceived == 60){command[4] = false;}
      if(transmitReceived == 7) {command[5] = true;}//slow down
      if(transmitReceived == 70){command[5] = false;}
      else{}
      
    return command;
    
 }//End checkCommand
 
 public static void move(boolean[]D, int S)
 { 
  int movingSpeed;
  boolean[] direction = new boolean[4];

  direction[0] = D[0];
  direction[1] = D[1];
  direction[2] = D[2];
  direction[3] = D[3];
  
  movingSpeed = S;

  motorA.setSpeed(movingSpeed);
  motorC.setSpeed(movingSpeed);
 
  
  if(direction[0] == true)
    {
     motorA.forward();  
      motorC.forward();
      }
  
  if(direction[1] == true)
    {
     motorA.backward();  
      motorC.backward();
      }
    
  if(direction[2] == true)
    {
      motorA.setSpeed(turnSpeed);
      motorC.setSpeed(turnSpeed);
      motorA.forward();
      motorC.backward();
    }
    
  if(direction[3] == true)
    {
      motorA.setSpeed(turnSpeed);
      motorC.setSpeed(turnSpeed);
      motorA.backward(); 
      motorC.forward();
    }
   
  if(direction[0] == true && direction[2] == true)
    {
      speedBuffer =  (int) (movingSpeed *1.5);
      
      motorA.setSpeed(speedBuffer);
      motorC.forward();
      motorA.forward();
    }
    
  if(direction[0] == true && direction[3] == true)
    {
      speedBuffer =  (int) (movingSpeed *1.5);
      
      motorC.setSpeed(speedBuffer);
      motorC.forward();
      motorA.forward();
      }
        
  if(direction[1] == true && direction[2] == true)
    {
      speedBuffer =  (int) (movingSpeed *1.5);
      
      motorA.setSpeed(speedBuffer);
      motorC.backward();
      motorA.backward();
    }
    
  if(direction[1] == true && direction[3] == true)
    {
      speedBuffer =  (int) (movingSpeed *1.5);
        
      motorC.setSpeed(speedBuffer);
      motorC.backward();
      motorA.backward();
    }
    
    if(direction[0] == false && direction[1] == false &&
       direction[2] == false && direction[3] == false)
    {
      motorA.stop();
      motorC.stop();
    }
   
 }//End move
 
 
 
 public static int getSpeed(boolean[] D)
 {
    boolean accelerate, decelerate;
  
     accelerate = D[4];
     decelerate = D[5];
     
     if(accelerate == true)
     {
        speed += 50;
        command[4] = false;
        }
     
     if(decelerate == true)
     {
        speed -= 50;
        command[5] = false;
        }
     
     return speed;
     }//End getSpeed
}





 
 
 

